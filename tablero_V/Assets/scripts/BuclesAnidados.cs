using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuclesAnidados : MonoBehaviour
{
    public GameObject casillaBlanca;
    public GameObject casillaNegra;

    public int ancho;
    public int alto;

    // Start is called before the first frame update
    void Start()
    {
        for (int j = 0; j < alto; j++)
        {
            for (int i = 0; i < ancho; i++)
            {
                if ((i+j) % 2 == 0)
                {
                    Instantiate(casillaBlanca, transform.position + new Vector3(i, j, 0), Quaternion.identity);
                }
                else
                {
                    Instantiate(casillaNegra, transform.position + new Vector3(i, j, 0), Quaternion.identity);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
