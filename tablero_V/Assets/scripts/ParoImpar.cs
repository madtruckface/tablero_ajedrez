using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParoImpar : MonoBehaviour
{
    public List<int> listaEnteros;
    public List<GameObject> listaGameObject;
    public int numerosAleatorios;

    // Start is called before the first frame update
    void Start()
    {
        int i = 0;
        while (i < numerosAleatorios)
        {
            listaEnteros.Add(Random.Range(0, 100));
            i++;
        }

        /*for (int k = 0; k < listaEnteros.Count; k++)
        {
            Debug.Log(listaEnteros[k]);
        }*/

        foreach (int numero in listaEnteros)
        {
            if (numero % 2 == 0)
            {
                Debug.Log(numero + " Es Par");
            }
            else
            {
                Debug.Log(numero + " Es Impar ");
            }
            
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
