using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tablero : MonoBehaviour
{
    public GameObject white;
    public GameObject black;
    public int contador;
    public float positionMultiplier;
    public float positionMultiplier2;


    // Start is called before the first frame update
    void Start()
    {
        for(int i=0; i<contador; i++)
        {
            Instantiate(white, positionMultiplier * new Vector3(1,0,0)*i, Quaternion.identity);
           // Instantiate(white, positionMultiplier * new Vector3(0, 1, 0) * 1, Quaternion.identity);
        }
        for(int j = 1; j < contador; j++)
        {
            Instantiate(black, positionMultiplier2 * new Vector3(1, 0, 0) * j, Quaternion.identity);
            //Instantiate(black, positionMultiplier2 * new Vector3(0, 1, 0) * j, Quaternion.identity);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
